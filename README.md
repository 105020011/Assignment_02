# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 
    level : 總共三個level, 一二是普通關卡, 擊落某數量的敵人後, 可進到下一關, 第三關是boss
    skill : 每擊落三個敵人, 可以有一次skill, 按'/'可使用, 清掉目前已出現的敵機(不包括boss)
    boss被擊中時會閃白光
    敵人飛機根據不同血量會有不同外觀
2. Animations : 
    玩家的飛機左右移動時有動畫
3. Particle Systems : 
    子彈擊中玩家和敵人時使用 particle system
4. Sound effects : 
    (1) 子彈擊中玩家和敵人時
    (2) 遊玩時
    在menu可調整音量
5. Leaderboard : 
    NO
6. Pause : 
    按'P'可以開始和暫停

# Bonus Functions Description : 
1. BOSS
2. 
3. 
