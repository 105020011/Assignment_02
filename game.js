// Initialize Phaser
var game = new Phaser.Game(500, 500, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = { score: 0 , P: 0, volume: 0.5};
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('failed', failedState);
game.state.add('congrat', congratState);
game.state.add('volumeSetting', volumeState);
// Start the 'boot' state
game.state.start('boot');
