var failedState = {
    create: function() {
        // Add a background image
        //game.add.image(0, 0, 'background');
    
        // Display the name of the game
        var resultLabel = game.add.text(game.width/2, 80, 'FAILED',
            { font: '50px Arial', fill: '#ffffff' });
        resultLabel.anchor.setTo(0.5, 0.5);
        
        // Show the score at the center of the screen
        var scoreLabel = game.add.text(game.width/2, game.height/2,
            'highest score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);
        
        var startButton = game.add.button(game.width/3, game.height-80, 'play', this.start, this, 1, 0);
        startButton.anchor.setTo(0.5, 0.5);

        var menuButton = game.add.button(game.width/3*2, game.height-80, 'menu', this.menu, this, 1, 0);
        menuButton.anchor.setTo(0.5, 0.5);
        
    },

    updata: function() {

    },

    start: function() {
        // Start the actual game
        game.state.start('play');
    },

    menu: function() {
        // Start the actual game
        game.state.start('menu');
    }
};