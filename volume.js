var volumeState = {
    create: function() {
            
        // Display the name of the game
        var nameLabel = game.add.text(game.width/2, 80, 'Volume',
            { font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);

        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        progressBar.scale.setTo(1.5,1); // 130*1.5 = 195

        this.setting = game.add.sprite(game.width/2, 200, 'set');
        this.setting.anchor.setTo(0.5, 0.5);
        game.physics.enable(this.setting, Phaser.Physics.ARCADE);

        var backButton = game.add.button(game.width/2, game.height-80, 'back', this.back, this, 1, 0);
        backButton.anchor.setTo(0.5, 0.5);
        
    },
    back: function() {
        // Start the actual game
        game.state.start('menu');
    },

    update: function() {

        //  only move when you click
        if (game.input.mousePointer.isDown && game.input.y < 250)
        {
            //console.log("x: " + (this.setting.body.x+16));
            //console.log("pointer x: " + game.input.x);
                
            if(game.input.x > game.width/2+90){
                this.setting.body.x = game.width/2+90-16;
                game.global.volume = 1;
                //console.log("volume: " + game.global.volume);
            }else if(game.input.x < game.width/2-90){
                this.setting.body.x = game.width/2-90-16;
                game.global.volume = 0;
                //console.log("volume: " + game.global.volume);
            }else{
                this.setting.body.x = game.input.x-16;
                game.global.volume = (game.input.x-(game.width/2-90-16))/180;
                //console.log("volume: " + game.global.volume);
            }
        }
        else
        {
            this.setting.body.velocity.setTo(0, 0);
        }
    
    }
}; 