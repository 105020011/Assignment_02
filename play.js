var playState = {
    
    create: function() {
        game.stage.backgroundColor = '#3498db';
        //game.add.image(0, 0, 'background');
        this.background = game.add.tileSprite(0, 0, 600, 500, 'background'); 

        this.cursor = game.input.keyboard.createCursorKeys();
        this.ultKey = game.input.keyboard.addKey(Phaser.Keyboard.QUESTION_MARK);
        this.pauseKey = game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.pauseKey.onDown.add(this.changeP, this);
        this.rocketKey = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);
        this.rocketKey.onDown.add(this.emitRocket, this);
        
        this.player = game.add.sprite(game.width/2, game.height-34, 'player');
        game.physics.arcade.enable(this.player);
        this.player.anchor.setTo(0.5, 0.5);
        this.player.life = 200;
        //this.player.facingLeft = false;

        this.lifeLabel = game.add.text(0, 0,
            'life: ' + this.player.life, { font: '25px Arial', fill: '#ffffff' });
        this.lifeLabel.anchor.setTo(0, 0);

        this.scoreNow = 0;
        this.scoreTotal = this.player.life;
        this.scoreLabel = game.add.text(game.width/3, 0,
            'score: ' + this.scoreTotal, { font: '25px Arial', fill: '#ffffff' });
        this.scoreLabel.anchor.setTo(0.5, 0);

        this.levelNow = 1;
        this.levelLabel = game.add.text(game.width/2-20, 0,
            'level: ' + this.levelNow, { font: '25px Arial', fill: '#ffffff' });
        this.levelLabel.anchor.setTo(0, 0);

        this.ultNum = 0;
        this.ultNumLabel = game.add.text(game.width/4*3, 0,
            'ult: ' + this.ultNum + '/3' , { font: '25px Arial', fill: '#ffffff' });
        this.scoreLabel.anchor.setTo(0.5, 0);

        this.pauseLabel = game.add.text(game.width, 0,
            '', { font: '25px Arial', fill: '#ffffff' })
        this.pauseLabel.anchor.setTo(1, 0);

        //add player bullet group
        this.playerBullets = game.add.group();
        this.playerBullets.enableBody = true;
        this.playerBullets.createMultiple(20, 'playerBullet');
        game.time.events.loop(100, this.addPlayerBullet, this);

        //add enemy
        this.enemys = game.add.group();
        this.enemys.enableBody = true;
        this.enemys.createMultiple(2, 'enemy');
        game.time.events.loop(2500, this.addEnemy, this);

        //add enemy2
        this.enemys2 = game.add.group();
        this.enemys2.enableBody = true;
        this.enemys2.createMultiple(2, 'enemy2');

        //add enemy bullet group
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.createMultiple(40, 'enemyBullet');
        game.time.events.loop(250, this.addEnemyBullet, this);
        
        this.rockets = game.add.group();
        this.rockets.enableBody = true;
        this.rockets.createMultiple(2, 'rocket');
        this.rocketNum = 1;

        this.bosses = game.add.group();
        this.bosses.enableBody = true;
        this.bosses.createMultiple(1, 'boss');

        /// ToDo 3: Add 4 animations.
        /// 1. Create the 'rightwalk' animation with frame rate = 8 by looping the frames 1 and 2
        this.player.animations.add('rightwalk', [2], 8, false);
        /// 2. Create the 'leftwalk' animation with frame rate = 8 by looping the frames 3 and 4
        this.player.animations.add('leftwalk', [1], 8, false);
        /// 3. Create the 'rightjump' animation with frame rate = 16 (frames 5 and 6 and no loop)
        //this.player.animations.add('rightjump', [5, 6], 16, false);
        /// 4. Create the 'leftjump' animation with frame rate = 16 (frames 7 and 8 and no loop)
        //this.player.animations.add('leftjump', [7, 8], 16, false);
        ///


        /// Add a little yellow block :)
        //this.yellowBlock = game.add.sprite(200, 320, 'block1');
        //this.yellowBlock.animations.add('Yblockanim', [0, 1, 2, 3], 8,  true);
        //game.physics.arcade.enable(this.yellowBlock);
        //this.yellowBlock.body.immovable = true;      
        
        /// Add a little dark blue block ;)
        //this.blueBlock = game.add.sprite(422, 320, 'block2');
        //this.blueBlock.animations.add('Bblockanim', [0, 1, 2, 3], 8,  true);
        //game.physics.arcade.enable(this.blueBlock);
        //this.blueBlock.body.immovable = true;
        

        /// Particle
        this.emitter = game.add.emitter(0, 0, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 0);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 0;

        this.emitter2 = game.add.emitter(0, 0, 15);
        this.emitter2.makeParticles('pixel');
        this.emitter2.setYSpeed(0, 150);
        this.emitter2.setXSpeed(-150, 150);
        this.emitter2.setScale(2, 0, 2, 0, 800);
        this.emitter2.gravity = 0;


        /// Add floor
        //this.floor = game.add.sprite(0, game.height - 30, 'ground'); 
        //game.physics.arcade.enable(this.floor);
        //this.floor.body.immovable = true;

        // Add vertical gravity to the player
        //this.player.body.gravity.y = 500;

        // Add music
        this.attackedSound = game.add.audio('attacked');
        this.bgmSound = game.add.audio('bgm');
        this.bgmSound.volume = game.global.volume;
        this.bgmSound.play();
        
    },
    update: function() {

        this.background.tilePosition.y += 1;

        /// ToDo 6: Add collision 
        /// 1. Add collision between player and walls
        //game.physics.arcade.collide(this.player, this.walls);
        /// 2. Add collision between player and floor
        //game.physics.arcade.collide(this.player, this.floor);
        /// 3. Add collision between player and yellowBlock and add trigger animation "blockTween"
        //game.physics.arcade.collide(this.player, this.yellowBlock, this.blockTween, null, this);
        /// 4. Add collision between player and blueBlock and add trigger animation "blockParticle"
        //game.physics.arcade.collide(this.player, this.blueBlock, this.blockParticle, null, this);
        //this.player.touching = { left: true, right: true };
        ///
        game.physics.arcade.overlap(this.player, this.enemyBullets, this.playerAttacked, null, this);
        game.physics.arcade.overlap(this.enemys, this.playerBullets, this.enemyAttacked, null, this);
        game.physics.arcade.overlap(this.enemys2, this.playerBullets, this.enemyAttacked, null, this);
        game.physics.arcade.overlap(this.bosses, this.playerBullets, this.bossAttacked, null, this);
        game.physics.arcade.overlap(this.playerBullets, this.enemyBullets, this.bulletColli, null, this);

        //if (!this.player.inWorld) {
        //    this.playerDie();
        //}
        if(!game.global.P) {
            this.movePlayer();
            this.ultSkill();
        }
        this.moveEnemy();
    }, 
    playerDie: function() { game.state.start('main');},

    playerAttacked: function(player, bullet) {
        //console.log("player attacked");
        bullet.kill();
        this.player.life -= 10;
        this.lifeLabel.text = 'life: ' + this.player.life;
        this.scoreTotal = this.scoreNow + this.player.life;
        this.scoreLabel.text = 'score: ' + this.scoreTotal;
        if(this.player.life == 0){
            if(this.scoreTotal > game.global.score) {
                game.global.score = this.scoreTotal;
            }
            this.bgmSound.stop();
            game.state.start('failed');
        }
        this.emitter.x = bullet.x;
        this.emitter.y = bullet.y;
        this.emitter.start(true, 800, null, 15);
        this.attackedSound.volume = game.global.volume;
        //console.log("volume: " + game.global.volume);
        this.attackedSound.play();
    },

    enemyAttacked: function(enemy, bullet) {
        //console.log("enemy attacked");
        bullet.kill();
        this.emitter2.x = bullet.x;
        this.emitter2.y = bullet.y;
        this.emitter2.start(true, 800, null, 15);
        enemy.life -= 20;
        if(enemy.life >= 70) {
            enemy.frame = 0;
        }else if(enemy.life >= 50) {
            enemy.frame = 1;
        }else if(enemy.life >= 30) {
            enemy.frame = 2;
        }else if(enemy.life == 0) {
            enemy.kill();
            this.scoreNow += 10;
            this.scoreTotal = this.scoreNow + this.player.life;
            this.scoreLabel.text = 'score: ' + this.scoreTotal;
            this.ultNum += 1;
            this.ultNumLabel.text = 'ult: ' + this.ultNum + '/3';
        }
        this.attackedSound.volume = game.global.volume;
        this.attackedSound.play();
    },

    bossAttacked: function(enemy, bullet) {
        //console.log("enemy attacked");
        bullet.kill();
        game.camera.flash(0xffffff, 300);
        enemy.life -= 10;
        if(enemy.life == 0) {
            enemy.kill();
            this.scoreNow += 10;
            this.scoreTotal = this.scoreNow + this.player.life;
            this.scoreLabel.text = 'score: ' + this.scoreTotal;
            if(this.scoreTotal > game.global.score) {
                game.global.score = this.scoreTotal;
            }
            this.ultNum += 1;
            this.ultNumLabel.text = 'ult: ' + this.ultNum + '/3';
            this.bgmSound.stop();
            game.state.start("congrat");
        }
        this.attackedSound.volume = game.global.volume;
        this.attackedSound.play();
    },

    bulletColli: function(playerB, enemyB) {
        playerB.kill();
        enemyB.kill();
    },

    /// ToDo 7: Finish the 4 animation part.
    movePlayer: function() {
        if (!game.global.P) {
            if(this.player.x>0) {
                this.player.body.velocity.x = -600;
                //console.log("player.x = " + this.player.x + " player.y = " + this.player.y);
                //this.player.facingLeft = true;

                /// 1. Play the animation 'leftwalk'
                this.player.animations.play('leftwalk');
                ///
            } else {
                this.player.body.velocity.x = 0;
                //console.log("left out");
                //console.log("width = " + game.width + " height = " + game.height);
                //console.log("player.x = " + this.player.x + " player.y = " + this.player.y);
            }
        }

        if (this.cursor.right.isDown) { 
            if(this.player.x<game.width) {
                this.player.body.velocity.x = 600;
                //console.log("player.x = " + this.player.x + " player.y = " + this.player.y);
                //this.player.facingLeft = false;

                /// 2. Play the animation 'rightwalk' 
                this.player.animations.play('rightwalk');
                ///
            } else {
                this.player.body.velocity.x = 0;
                //console.log("right out");
                //console.log("width = " + game.width + " height = " + game.height);
                //console.log("player.x = " + this.player.x + " player.y = " + this.player.y);
            }
        }   

        // If the up arrow key is pressed, And the player is on the ground.
        if (this.cursor.up.isDown) { 
            if(this.player.y>0) {
            //if(this.player.body.touching.down){
                // Move the player upward (jump)
                //if(this.player.facingLeft) {
                    /// 3. Play the 'leftjump' animation
                    //this.player.animations.play('leftjump');
                    ///
                //}else {
                    /// 4. Play the 'rightjump' animation
                    //this.player.animations.play('rightjump');
                    ///
                //}
                this.player.body.velocity.y = -100;
           // }
            } else {
                //console.log("up out");
                this.player.body.velocity.y = 0;
            }
        }
        
        //If the down key is pressed
        if (this.cursor.down.isDown) {
            if(this.player.y<game.height) {
                this.player.body.velocity.y = 100;
            } else {
                //console.log("down out");
                this.player.body.velocity.y = 0;
            }
        }

        // If neither the right or left arrow key is pressed
        if (!this.cursor.left.isDown && !this.cursor.right.isDown) {
            // Stop the player 
            this.player.body.velocity.x = 0;
        
            //if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 0;
            //}else {
                // Change player frame to 1 (Facing right)
                //this.player.frame = 1;
            //}

            // Stop the animation
            this.player.animations.stop();
        }   
        
        if(!this.cursor.up.isDown && !this.cursor.down.isDown) {
            this.player.body.velocity.y = 0;
        }
    },

    moveEnemy: function() {
        this.enemys.forEach(function(enemy) {
            if(enemy.alive == true) {
                //console.log("velocity: " + enemy.body.velocity.x);
                if(!game.global.P) {
                    if(enemy.body.x+34 >= game.width) {
                        //console.log("left, enemy.x = " + enemy.body.x);
                        enemy.dir = -1;
                        enemy.body.velocity.x = -100;
                        //enemy.frame = 1;
                    }else if(enemy.body.x+34 <= 0){
                        //console.log("right, enemy.x = " + enemy.body.x);
                        enemy.dir = 1;
                        enemy.body.velocity.x = 100;
                        //enemy.frame = 2;
                    } else if (enemy.dir == -1) {
                        //console.log("left");
                        enemy.body.velocity.x = -100;
                        //enemy.frame = 1;
                    } else {
                        //console.log("right");
                        enemy.body.velocity.x = 100;
                        //enemy.frame = 2;
                    }
                } else {
                    enemy.body.velocity.x = 0;
                    //console.log("enemy cannot move");
                }
            } 
        }, this);

        this.enemys2.forEach(function(enemy) {
            if(enemy.alive == true) {
                if(!game.global.P) {
                    if(enemy.body.x+34 >= game.width) {
                        //console.log("enemy.x = " + enemy.body.x);
                        enemy.dir = -1;
                        enemy.body.velocity.x = -200;
                        //enemy.frame = 1;
                    }else if(enemy.body.x+34 <= 0){
                        //console.log("enemy.x = " + enemy.body.x);
                        enemy.dir = 1;
                        enemy.body.velocity.x = 200;
                        //enemy.frame = 2;
                    } else if (enemy.dir == -1) {
                        //console.log("enemy.x = " + enemy.body.x);
                        enemy.body.velocity.x = -200;
                        //enemy.frame = 1;
                    } else {
                        //console.log("enemy.x = " + enemy.body.x);
                        enemy.body.velocity.x = 200;
                        //enemy.frame = 2;
                    }
                } else {
                    enemy.body.velocity.x = 0;
                    //console.log("enemy cannot move");
                }
            }
        }, this);

        this.bosses.forEach(function(enemy) {
            if(enemy.alive == true) {
                if(!game.global.P) {
                    if(enemy.body.y >= 100) {
                        //console.log("enemy.y = " + enemy.body.y);
                        enemy.dir = -1;
                        enemy.body.velocity.y = -10;
                        //enemy.frame = 1;
                    }else if(enemy.body.y <= 30){
                        //console.log("enemy.y = " + enemy.body.y);
                        enemy.dir = 1;
                        enemy.body.velocity.y = 10;
                        //enemy.frame = 2;
                    } else if (enemy.dir == -1) {
                        //console.log("enemy.x = " + enemy.body.x);
                        enemy.body.velocity.y = -10;
                        //enemy.frame = 1;
                    } else {
                        //console.log("enemy.x = " + enemy.body.x);
                        enemy.body.velocity.y = 10;
                        //enemy.frame = 2;
                    }
                } else {
                    enemy.body.velocity.y = 0;
                    //console.log("enemy cannot move");
                }
            }
        }, this);
    },

    ultSkill: function(){
        if(this.ultKey.isDown){
            if(!game.global.P) {
                if(this.ultNum >= 3){
                    this.ultNum -= 3;
                    this.ultNumLabel.text = 'ult: ' + this.ultNum + '/3';
                    this.enemys.forEach(function(enemy) {
                        enemy.kill();
                    }, this);
                    this.enemys2.forEach(function(enemy) {
                        enemy.kill();
                    }, this);
                }
            }
        }
    },

    addPlayerBullet: function() {
        if(!game.global.P) {
            // Get the first dead enemy of the group
            var bullet = this.playerBullets.getFirstDead();
            // If there isn't any dead enemy, do nothing
            if (!bullet) { return;}
            /* Initialize enemy */
            // Set the anchor point centered at the bottom
            bullet.anchor.setTo(0.5, 0.5);
            // Put the enemy above the top hole
            bullet.reset(this.player.x, this.player.y-34);
            // Add velocity to bullet
            bullet.body.velocity.y = -600;
            bullet.checkWorldBounds = true;
            bullet.outOfBoundsKill = true;
        }
    },

    addEnemy: function() {
        if(!game.global.P) {
            if(this.scoreNow <= 40) {
                this.levelNow = 1;
                this.levelLabel.text = 'level: ' + this.levelNow;
                // Get the first dead enemy of the group
                var enemy = this.enemys.getFirstDead();
                // If there isn't any dead enemy, do nothing
                if (!enemy) { return;}
                /* Initialize enemy */
                // Set the anchor point centered at the center
                enemy.anchor.setTo(0.5, 0.5);
                // Put the enemy above the top hole
                enemy.reset(game.width/2, 68);
                // reset enemy life
                enemy.life = 60;
                //reset enemy frame
                enemy.frame = 0;
                // Add velocity to bullet
                enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
                if(enemy.body.velocity.x == 100) {
                    enemy.dir = 1;
                } else {
                    enemy.dir = -1;
                }
            }else if(this.scoreNow <= 80) {

                this.levelNow = 2;
                this.levelLabel.text = 'level: ' + this.levelNow;

                // check enemy one have been defeated
                var clear = 1;
                this.enemys.forEach(function(enemy){
                    if(enemy.alive == true){
                        clear = 0;
                    }
                },this);

                if(clear==1){
                    // Get the first dead enemy of the group
                    var enemy = this.enemys2.getFirstDead();
                    // If there isn't any dead enemy, do nothing
                    if (!enemy) { return;}
                    /* Initialize enemy */
                    // Set the anchor point centered at the center
                    enemy.anchor.setTo(0.5, 0.5);
                    // Put the enemy above the top hole
                    enemy.reset(game.width/2, 68);
                    // reset enemy life
                    enemy.life = 100;
                    //reset enemy frame
                    enemy.frame = 0;
                    // Add velocity to bullet
                    enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
                    if(enemy.body.velocity.x == 100) {
                        enemy.dir = 1;
                    } else {
                        enemy.dir = -1;
                    }
                }
            } else {
                this.levelNow = 3;
                this.levelLabel.text = 'level: ' + 'BOSS';

                // check enemy one have been defeated
                var clear2 = 1;
                this.enemys2.forEach(function(enemy){
                    if(enemy.alive == true){
                        clear2 = 0;
                    }
                },this);

                if(clear2==1){
                    // Get the first dead enemy of the group
                    var boss = this.bosses.getFirstDead();
                    // If there isn't any dead enemy, do nothing
                    if (!boss) { return;}
                    /* Initialize enemy */
                    // Set the anchor point centered at the center
                    boss.anchor.setTo(0.5, 0);
                    boss.scale.setTo(1.2, 0.5);
                    // Put the enemy above the top hole
                    boss.reset(game.width/2, 30);
                    boss.body.velocity.y = 10;
                    boss.dir = 1;
                    // reset enemy life
                    boss.life = 1000;
                }
            }
        }
    },

    addEnemyBullet: function() {
        if(!game.global.P) {
            this.enemys.forEach(function(enemy) {
                if(enemy.alive == true) {
                    // Get the first dead enemy of the group
                    var bullet = this.enemyBullets.getFirstDead();
                    // If there isn't any dead enemy, do nothing
                    if (!bullet) { return;}
                    /* Initialize enemy */
                    // Set the anchor point centered at the center
                    bullet.anchor.setTo(0.5, 0.5);
                    // Put the enemy above the top hole
                    bullet.reset(enemy.x, enemy.y+34);
                    // Add velocity to bullet
                    bullet.body.velocity.y = 150;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                }
            }, this);

            this.enemys2.forEach(function(enemy) {
                if(enemy.alive == true) {
                    // Get the first dead enemy of the group
                    var bullet = this.enemyBullets.getFirstDead();
                    // If there isn't any dead enemy, do nothing
                    if (!bullet) { return;}
                    /* Initialize enemy */
                    // Set the anchor point centered at the center
                    bullet.anchor.setTo(0.5, 0.5);
                    // Put the enemy above the top hole
                    bullet.reset(enemy.x-15, enemy.y+34);
                    // Add velocity to bullet
                    bullet.body.velocity.y = 150;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                    // Get the first dead enemy of the group
                    var bullet2 = this.enemyBullets.getFirstDead();
                    // If there isn't any dead enemy, do nothing
                    if (!bullet2) { return;}
                    /* Initialize enemy */
                    // Set the anchor point centered at the center
                    bullet2.anchor.setTo(0.5, 0.5);
                    // Put the enemy above the top hole
                    bullet2.reset(enemy.x+15, enemy.y+34);
                    // Add velocity to bullet
                    bullet2.body.velocity.y = 150;
                    bullet2.checkWorldBounds = true;
                    bullet2.outOfBoundsKill = true;
                }
            }, this);

            this.bosses.forEach(function(boss) {
                if(boss.alive == true){
                    //console.log("boss bullet");
                    // Get the first dead enemy of the group
                    var bullet = this.enemyBullets.getFirstDead();
                    // If there isn't any dead enemy, do nothing
                    if (!bullet) { return;}
                    /* Initialize enemy */
                    // Set the anchor point centered at the center
                    bullet.anchor.setTo(0.5, 0.5);
                    // Put the enemy above the top hole
                    bullet.reset(boss.x, boss.y+80);
                    // Add velocity to bullet
                    bullet.body.velocity.y = 150;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;

                    bullet = this.enemyBullets.getFirstDead();
                    if (!bullet) { return;}
                    bullet.anchor.setTo(0.5, 0.5);
                    bullet.reset(boss.x+110, boss.y+200);
                    bullet.body.velocity.x = 80;
                    bullet.body.velocity.y = 100;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;

                    bullet = this.enemyBullets.getFirstDead();
                    if (!bullet) { return;}
                    bullet.anchor.setTo(0.5, 0.5);
                    bullet.reset(boss.x-100, boss.y+200);
                    bullet.body.velocity.x = -80;
                    bullet.body.velocity.y = 100;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;

                    bullet = this.enemyBullets.getFirstDead();
                    if (!bullet) { return;}
                    bullet.anchor.setTo(0.5, 0.5);
                    bullet.reset(boss.x-20, boss.y+80);
                    bullet.body.velocity.x = -40;
                    bullet.body.velocity.y = 130;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                    
                    bullet = this.enemyBullets.getFirstDead();
                    if (!bullet) { return;}
                    bullet.anchor.setTo(0.5, 0.5);
                    bullet.reset(boss.x+30, boss.y+80);
                    bullet.body.velocity.x = 40;
                    bullet.body.velocity.y = 130;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                    
                    bullet = this.enemyBullets.getFirstDead();
                    if (!bullet) { return;}
                    bullet.anchor.setTo(0.5, 0.5);
                    bullet.reset(boss.x-20, boss.y+80);
                    bullet.body.velocity.x = -10;
                    bullet.body.velocity.y = 140;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;

                    bullet = this.enemyBullets.getFirstDead();
                    if (!bullet) { return;}
                    bullet.anchor.setTo(0.5, 0.5);
                    bullet.reset(boss.x+30, boss.y+80);
                    bullet.body.velocity.x = 10;
                    bullet.body.velocity.y = 140;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;

                    bullet = this.enemyBullets.getFirstDead();
                    if (!bullet) { return;}
                    bullet.anchor.setTo(0.5, 0.5);
                    bullet.reset(boss.x-100, boss.y+200);
                    bullet.body.velocity.x = -130;
                    bullet.body.velocity.y = 10;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                    
                    bullet = this.enemyBullets.getFirstDead();
                    if (!bullet) { return;}
                    bullet.anchor.setTo(0.5, 0.5);
                    bullet.reset(boss.x+100, boss.y+200);
                    bullet.body.velocity.x = 130;
                    bullet.body.velocity.y = 10;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                }
            }, this);
        }
    },

    changeP: function() {
        if(game.global.P) {
            game.global.P = 0;
            this.pauseLabel.text = '';
            //console.log("pause: " + game.global.P);

            // start bullet
            this.playerBullets.forEach(function(bullet) {
                if(bullet.alive == true) {
                    // reset velocity to bullet
                    bullet.body.velocity.y = -600;
                }
            }, this);

            this.enemyBullets.forEach(function(bullet) {
                if(bullet.alive == true) {
                    // reset velocity to bullet
                    bullet.body.velocity.y = 150;
                }
            }, this);

        } else {
            game.global.P = 1;
            this.pauseLabel.text = 'P';
            //console.log("pause: " + game.global.P);

            // stop bullet
            this.playerBullets.forEach(function(bullet) {
                if(bullet.alive == true) {
                    // reset velocity to bullet
                    bullet.body.velocity.y = 0;
                }
            }, this);

            this.enemyBullets.forEach(function(bullet) {
                if(bullet.alive == true) {
                    // reset velocity to bullet
                    bullet.body.velocity.y = 0;
                    bullet.body.velocity.x = 0;
                }
            }, this);
        }
    },

    emitRocket: function() {
        if(this.rocketNum>0){
            //console.log("emit rocket");
            this.enemys.forEach(function(enemy) {
                if(enemy.alive == true) {
                    //console.log("find target");
                    var rocket = this.rockets.getFirstDead();
                    if(!rocket) {return;};
                    rocket.anchor.setTo(0.5, 0.5);
                    rocket.scale.setTo(0.3, 0.3);
                    // Put the enemy above the top hole
                    rocket.reset(this.player.x+15, this.player.y+34);
                    rocket.checkWorldBounds = true;
                    rocket.outOfBoundsKill = true;
                    //console.log("fire");
                    this.game.physics.arcade.moveToObject(rocket, enemy, 600);
                    //this.rocketNum--;
                    return;
                }
            }, this);
        }
    }
  
};