var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,
            'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        
        // Load all game assets
        game.load.spritesheet('player', 'assets/player_sprite.png',87, 68);
        game.load.image('playerBullet', 'assets/player_bullet.png');
        game.load.spritesheet('enemy', 'assets/enemy.png', 83, 61);
        game.load.spritesheet('enemy2', 'assets/enemy2.png', 89, 61);
        game.load.image('enemyBullet', 'assets/enemy_bullet.png');
        game.load.spritesheet('play', 'assets/play.png',150, 61);
        game.load.spritesheet('volume', 'assets/volume.png',150, 61);
        game.load.spritesheet('back', 'assets/back.png',150, 61);
        game.load.spritesheet('menu', 'assets/menu.png',150, 61);
        game.load.spritesheet('rocket', 'assets/rocket.png',67, 144);
        game.load.image('boss', 'assets/boss.png');
        game.load.image('set', 'assets/set.png');
        
        // Load a new asset that we will use in the menu state
        game.load.image('background', 'assets/background.jpg');

        // Loat game sprites.
        //game.load.image('background', 'assets/background.png');
        //game.load.image('ground', 'assets/ground.png');
        game.load.image('pixel', 'assets/pixel.png');



        /// Load block spritesheet.
        //game.load.spritesheet('block1', 'assets/block1.png', 28, 28);
        //game.load.spritesheet('block2', 'assets/block2.png', 28, 28);
        

        /// ToDo 1: Load spritesheet
        ///      The name of sprite is 'player'.
        ///      The spritesheet filename is assets/MARIO.png
        ///      A frame size is 32 x 54.
        //game.load.spritesheet('player', 'assets/MARIO.png', 32, 54);

        // load music
        game.load.audio('attacked', ['assets/hit.ogg', 'assets/hit.mp3']);
        game.load.audio('bgm', ['assets/bgm.ogg', 'assets/bgm.mp3']);
    },
    create: function() {
        // Go to the menu state
        game.state.start('menu');
    }
}; 